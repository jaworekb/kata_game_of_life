<?php

declare(strict_types=1);

namespace KataGameOfLife;

class TextToGridMapper
{
    const ALIVE_CHAR = '*';
    const DEAD_CHAR = '.';

    public function grid(string $string): Grid
    {
        if (empty($string)) {
            return new Grid();
        }

        $gridArray = [];
        foreach (\explode("\n", $string) as $i => $line) {
            $gridArray[$i] = [];

            foreach (\str_split($line) as $char) {
                if (!empty($char)) {
                    $alive = ($char == self::ALIVE_CHAR);
                    $gridArray[$i][] = new Cell($alive);
                }
            }
        }
        return new Grid($gridArray);
    }

    public function text(Grid $grid): string
    {
        $gridString = '';
        foreach ($grid as $row) {
            foreach ($row as $cell) {
                $gridString .= $cell->isAlive() ? self::ALIVE_CHAR : self::DEAD_CHAR;
            }
            $gridString .= "\n";
        }

        return $gridString;
    }
}