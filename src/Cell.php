<?php

declare(strict_types=1);

namespace KataGameOfLife;

class Cell
{
    private $alive;

    public function __construct($alive = true)
    {
        $this->alive = $alive;
    }

    public function makeAlive()
    {
        $this->alive = true;
    }

    public function makeDeath()
    {
        $this->alive = false;
    }

    public function isDeath(): bool
    {
        return !$this->alive;
    }

    public function isAlive(): bool
    {
        return $this->alive;
    }

    public function nextGeneration(int $liveNeighbours): Cell
    {
        if ($this->isAlive() && ($liveNeighbours < 2 || $liveNeighbours > 3)) {
            return new Cell(false);
        } elseif ($this->isAlive()) {
            return new Cell(true);
        } elseif ($this->isDeath() && $liveNeighbours == 3) {
            return new Cell(true);
        } else {
            return new Cell(false);
        }
    }
}