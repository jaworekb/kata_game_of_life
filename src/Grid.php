<?php

declare(strict_types=1);

namespace KataGameOfLife;

class Grid implements \IteratorAggregate
{
    private $cells;

    private $sizeX;

    private $sizeY;

    public function __construct(array $cells = [])
    {
        $this->cells = $cells;
        $this->sizeX = \count($cells[0] ?? []);
        $this->sizeY = \count($cells);
    }

    public function nextGeneration(): Grid
    {
        $newGrid = [];
        foreach ($this->cells as $i => $row) {
            foreach ($row as $j => $cell) {
                $newGrid[$i][$j] = $cell->nextGeneration($this->countAliveNeighbors($j, $i));
            }
        }

        return new Grid($newGrid);
    }

    public function countAliveNeighbors(int $x, int $y): int
    {
        $count = 0;
        $positions = [
            [$x - 1, $y],
            [$x - 1, $y + 1],
            [$x - 1, $y - 1],
            [$x, $y + 1],
            [$x, $y - 1],
            [$x + 1, $y],
            [$x + 1, $y + 1],
            [$x + 1, $y - 1],
        ];

        foreach ($positions as $position) {
            $cell = $this->get($position[0], $position[1]);
            if (isset($cell) && $cell->isAlive()) {
                $count += 1;
            }
        }

        return $count;
    }

    private function get(int $x, int $y): ?Cell
    {
        if ($this->positionValid($x, $y)) {
            return $this->cells[$y][$x];
        }

        return null;
    }

    private function positionValid(int $x, int $y)
    {
        return $x >= 0 && $x < $this->sizeX && $y >= 0 && $y < $this->sizeY;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->cells);
    }
}