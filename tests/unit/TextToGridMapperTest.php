<?php

declare(strict_types=1);

use KataGameOfLife\Cell;
use KataGameOfLife\Grid;
use KataGameOfLife\TextToGridMapper;
use PHPUnit\Framework\TestCase;

class TextToGridMapperTest extends TestCase
{
    /**
     * @dataProvider grid_strings_provider
     */
    public function test_returns_grid_when_string_given($stringToMap, $expectedGrid)
    {
        $mapper = new TextToGridMapper();

        $grid = $mapper->grid($stringToMap);

        $this->assertEquals($expectedGrid, $grid);
    }

    public function grid_strings_provider()
    {
        return [
            [
                '',
                new Grid(),
            ],
            [
                "*",
                new Grid([
                    [new Cell()],
                ]),
            ],
            [
                "*.",
                new Grid([
                    [new Cell(), new Cell(false)],
                ]),
            ],
            [
                "*\n*",
                new Grid([
                    [new Cell()],
                    [new Cell()],
                ]),
            ],
            [
                ".*.\n**.",
                new Grid([
                    [new Cell(false), new Cell(), new Cell(false)],
                    [new Cell(), new Cell(), new Cell(false)],
                ]),
            ],
        ];
    }

    /**
     * @dataProvider grid_provider
     */
    public function test_transforms_grid_to_text($grid, $expectedString)
    {
        $mapper = new TextToGridMapper();

        $text = $mapper->text($grid);

        $this->assertEquals($expectedString, $text);
    }

    public function grid_provider()
    {
        return [
            [
                new Grid(),
                '',
            ],
            [
                new Grid([
                    [new Cell(true)],
                ]),
                "*\n",
            ],
            [
                new Grid([
                    [new Cell(true)],
                    [new Cell(false)],
                ]),
                "*\n.\n",
            ],
            [
                new Grid([
                    [new Cell(true), new Cell(false)],
                    [new Cell(false), new Cell(true)],
                ]),
                "*.\n.*\n",
            ],
        ];
    }
}
