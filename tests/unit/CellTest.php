<?php

use KataGameOfLife\Cell;
use PHPUnit\Framework\TestCase;

class CellTest extends TestCase
{
    public function test_is_dead_when_was_made_death()
    {
        $cell = new Cell();

        $cell->makeDeath();

        $this->assertTrue($cell->isDeath());
    }

    public function test_is_alive_when_was_made_alive()
    {
        $cell = new Cell();

        $cell->makeAlive();

        $this->assertTrue($cell->isAlive());
    }

    /**
     * @dataProvider next_generation_neighborhood_provider
     */
    public function test_generates_next_generation_when_neighborhood_is_given(
        $alive,
        $liveNeighbours,
        $expectedCell
    ) {
        $cell = new Cell($alive);

        $nextGenerationCell = $cell->nextGeneration($liveNeighbours);

        $this->assertEquals($nextGenerationCell, $expectedCell);
    }

    public function next_generation_neighborhood_provider()
    {
        return [
            [true, 0, new Cell(false)],
            [true, 4, new Cell(false)],
            [true, 2, new Cell(true)],
            [false, 3, new Cell(true)],
        ];
    }
}
