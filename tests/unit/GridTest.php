<?php

use KataGameOfLife\Cell;
use KataGameOfLife\Grid;
use PHPUnit\Framework\TestCase;

class GridTest extends TestCase
{
    /**
     * @dataProvider grid_for_live_neighbours_calculation_provider
     */
    public function test_calculates_live_neighbours_when_position_given($x, $y, $expectedAlive)
    {
        $grid = new Grid(
            [
                [new Cell(false), new Cell(false), new Cell(true), new Cell(false), new Cell(false)],
                [new Cell(false), new Cell(true), new Cell(true), new Cell(true), new Cell(false)],
                [new Cell(false), new Cell(true), new Cell(true), new Cell(true), new Cell(false)],
                [new Cell(false), new Cell(true), new Cell(true), new Cell(true), new Cell(false)],
                [new Cell(false), new Cell(false), new Cell(true), new Cell(false), new Cell(false)],
            ]
        );

        $alive = $grid->countAliveNeighbors($x, $y);

        $this->assertEquals($expectedAlive, $alive);
    }

    public function grid_for_live_neighbours_calculation_provider()
    {
        return [
            [0, 0, 1],
            [2, 2, 8],
            [2, 0, 3],
            [2, 4, 3],
        ];
    }
}
