<?php

require '../vendor/autoload.php';

$text = <<<'EOT'
........
....*...
...**...
........
EOT;

//read
$grid = (new \KataGameOfLife\TextToGridMapper())->grid($text);

//generate
$newGrid = $grid->nextGeneration();

//write
echo (new \KataGameOfLife\TextToGridMapper())->text($newGrid);